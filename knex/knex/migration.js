const config =
	{
		development:
		{
			client: 'mysql',
			connection: {
				host: 'freemius-mysql',
				user: 'root',
				password: 'rootpassword',
				database: 'local_dev',
				charset   : 'utf8mb4',
				collate: 'utf8mb4_general_ci'
			},
			acquireConnectionTimeout: 8000,
			debug: false
		},

		production:
		{
			client: 'mysql',
			connection: {
				host: 'freemius-mysql',
				user: 'root',
				password: 'rootpassword',
				database: 'local_prod',
				charset   : 'utf8mb4',
				collate: 'utf8mb4_general_ci'
			},
			debug: false
		}
	};

require('mysql-import').config({
  'host': config.development.connection.host,
  'user': config.development.connection.user,
  'password': config.development.connection.password,
  'database': config.development.connection.database,
  'onerror': error => console.log(error)
}).import('./development/init.sql').then(() => {
  console.log('all statements have been executed');
  process.exit(1);
})
