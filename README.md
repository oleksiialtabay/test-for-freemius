# Test for FREEMIUS

----

## How to run

Both production and development environments are created using [Docker](https://www.docker.com/). All containers are isolated from OS and described in [docker-compose](https://docs.docker.com/compose/install/) file. It means that you can run it on Windows / Linux / MacOS at the same way. 

There is only difference is ENV variables that allows user to configure local, staging or production settings.

Open terminal and get to the project root folder. Run `docker-compose up` for dev environment or `docker-compose -f docker-compose-knex.yml up` to run database migrations. If you want to run it as a background process just add -d key at the end of the command. 

## Additional tools
There is several tools that helps developers or admins work with project included.
- [Adminer](https://www.adminer.org/) intended to handle the administration of MySQL over the Web. phpMyAdmin supports a wide range of operations on MySQL and MariaDB. You can get to it by url `{{domain}}/adminer`.

## Task 1
### Adding an item to a selected list
- URL: `POST http://localhost/api/items`
- payload: `{"name":"Querty","list_id":"1"}`

## Task 2
### Returning the last item inserted into a selected list
- URL: `GET http://localhost/api/items/latest`

## Task 3
### SPA
- URL: `GET http://localhost/`

Made using react and hooks. All request processes by NodeJS API.

#### Have a nice day!

