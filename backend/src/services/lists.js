const knex = require('./knex');
const TABLE_NAME = 'lists';

const service = {
  select: (limit, offset, options = {}) => {
    return knex(TABLE_NAME)
      .offset(offset)
      .limit(limit);
  },

  selectById: function(id) {
    return knex(TABLE_NAME)
      .where({id})
      .limit(1);
  },

  insert: function(data) {
    return knex(TABLE_NAME)
      .insert(data);
  },

  update: function(data) {
    return knex(TABLE_NAME)
      .where({id: data.id})
      .update(data);
  },

  delete: id => {
    return knex(TABLE_NAME)
      .where({id})
      .delete();
  },
};

module.exports = service;
