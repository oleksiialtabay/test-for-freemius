const knex = require('./knex');
const TABLE_NAME = 'items';

const service = {
  select: (limit, offset, options = {}) => {
    return knex(TABLE_NAME)
      .offset(offset)
      .limit(limit);
  },

  selectById: id => {
    return knex(TABLE_NAME)
      .where({id})
      .limit(1);
  },

  insert: data => {
    return knex(TABLE_NAME)
      .insert(data);
  },

  update: data => {
    return knex(TABLE_NAME)
      .where({id: data.id})
      .update(data);
  },

  delete: id => {
    return knex(TABLE_NAME)
      .where({id})
      .delete();
  },

  selectByListId: list_id => {
    return knex(TABLE_NAME)
      .where({list_id});
  },

  selectLatest: () => {
    return knex(TABLE_NAME)
      .orderBy('id')
      .limit(1)
      .then(models => models && models[0]);
  }
};

module.exports = service;
