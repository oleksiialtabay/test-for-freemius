const env = process.env.NODE_ENV;
const isDevEnviroment = ['development', 'test', 'dev'].includes(process.env.NODE_ENV);

const port = process.env.DB_PORT || 3306;
const host = process.env.DB_HOST;
const user = process.env.DB_USERNAME;
const password = process.env.DB_PASSWORD;
const database = process.env.DB_DATABASE;

const knex = require('knex')({
	client: 'mysql',
	connection: {
		host,
		user,
		password,
		database
	},
	acquireConnectionTimeout: 8000,
	debug: isDevEnviroment && false,
	pool: { min: 0, max: 7 }
});

module.exports = knex;

