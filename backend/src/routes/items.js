const express = require('express');
const router = express.Router();
const service = require('../services/items');

class DTO {
  constructor (data) {
    if (data.id) {
      this.id = data.id;
    }
    this.name = data.name;
    this.list_id = parseInt(data.list_id);
  }
}

router.get('/', (req, res, next) => {
  const query = {
    limit: +req.query.limit,
    offset: +req.query.offset,
  }

  service.select(query.limit, query.offset).then(models => {
    res.json( models.map(model => new DTO(model)) );
  }).catch(next);
});

router.get('/latest', (req, res, next) => {
  service.selectLatest().then(response => {

    if (!response) {
      response = null;
    }

    res.json( response );

  }).catch(next);
});

router.get('/list_id/:id', (req, res, next) => {
  const id = parseInt(req.params.id);

  service.selectByListId(id).then(response => {

    if (!response) {
      response = null;
    }

    res.json( response );

  }).catch(next);
});

router.get('/:id', (req, res, next) => {
  const id = req.params.id;
  service.selectById(id).then(models => {
    const response = Array.isArray(models) ? models[0] : models;

    if (!response) {
      response = null;
    }

    res.json( response );

  }).catch(next);
});

router.post('/', (req, res, next) => {
  const body = req.body;
  const request = Array.isArray(body) ?
    body.map(data => new DTO(data)) :
    new DTO(body);

  return service.insert(request).then(response => {
    res.json(response);
  }).catch(next);
});

router.put('/', (req, res, next) => {
  return service.update(new DTO(req.body)).then((id) => {
    res.json(id);
  }).catch(next);
});

router.delete('/:id', (req, res, next) => {
  const id = req.params.id;
  return service.delete(id).then(response => {
    res.json(response);
  }).catch(next);
});

module.exports = router;
