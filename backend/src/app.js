// ENV
const isDevEnviroment = ['development', 'test', 'dev'].includes(process.env.NODE_ENV);
if (isDevEnviroment) {
  process.env.DEBUG = 'dev';
}

const rootURL = '/api/';

// Dependencies
const express = require('express');
const sassMiddleware = require('node-sass-middleware');
const sass = require('node-sass');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const debug = require('debug')('dev');
const expressSession = require('express-session');
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// run logger on development env
if (isDevEnviroment) {
  // show logs
  app.use(logger('dev'));
}

// use middleware
app.use( bodyParser.urlencoded({ extended: true }) );
app.use( bodyParser.json() );
app.use( cookieParser() );
app.use( expressSession({ secret: 'freemius-secret', resave: true, saveUninitialized: true }) );

app.use(
  '/css',
  sassMiddleware({
    debug: process.env.NODE_ENV === 'development',
    dest: path.join(__dirname, 'public', 'css'),
    force: true,
    indentedSyntax: true,
    outputStyle: 'compressed',
    src: path.join(__dirname, 'public', 'sass')
  })
);

// headers
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
  next();
});

const routes = [
  'items',
  'lists',
];

routes.forEach(route => {
  app.use(
    rootURL + route,
    require('./routes/' + route)
  );
});

app.use(function(req, res, next) {
  res.render('index');
});

// error handler
app.use(function(err, req, res, next) {
  const isJson = req.is('application/json');

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);

  const errorObject = {
      Error: true,
      status: err.status,
      message: err.message,
      stack: err.stack
    };

  if (isJson) {
    res.json(errorObject);
    return;
  }

  if (err.code === 'ER_DUP_ENTRY') {
    errorObject.message = err.sqlMessage;
  }

  switch (err.status) {
    case 404:
      res.render('404', errorObject);
      break;

    default:
      res.render('error', errorObject);
      break;
  }

});

module.exports = app;
