import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import ErrorBoundary from './components/ErrorBoundary';
import Items from './pages/Items';
import Lists from './pages/Lists';
import CreateItem from './pages/CreateItem';

export default function routes() {
  return (
    <ErrorBoundary>
      <Router>
        <Switch>
          <Route path="/items/:list_id/new">
            <CreateItem />
          </Route>

          <Route path="/items/list_id/:list_id">
            <Items />
          </Route>

          <Route path="/">
            <Lists />
          </Route>

        </Switch>
      </Router>
    </ErrorBoundary>
  );
}

