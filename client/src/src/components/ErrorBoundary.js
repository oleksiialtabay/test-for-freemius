import React from "react";

class ErrorBoundary extends React.Component {
  state = {
    error: null,
  };

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { error };
  }

  componentDidCatch(error, errorInfo) {
  }

  render() {
    if (this.state.error) {
      return <h1>{ this.state.error.message }</h1>;
    }

    return this.props.children; 
  }
}

export default ErrorBoundary;
