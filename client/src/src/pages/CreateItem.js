import React, {useState} from 'react';
import {Link, useParams} from 'react-router-dom';
import { API_ROOT } from '../constants';

const CreateItem = () => {
  const {list_id} = useParams();
  const [name, setName] = useState('');
  const [isComplete, setIsComplete] = useState('');

  const handleComplete = () => {
    setIsComplete(true);
  }

  const handleChange = event => {
    setName(event.target.value);
  };

  const handleSubmit = event => {
    event.preventDefault();

    const headers = new Headers({
      'Content-Type': 'application/json'
    });

    const body = JSON.stringify({name, list_id});

    fetch(API_ROOT + 'items', {method: "POST", headers, body})
      .then(handleComplete)
      .catch(error => {
        throw error
      });
  };

  return isComplete ? (
      <>
        <Link to="/">Item addad. Go back</Link>
      </>
    ) : (
      <form onSubmit={handleSubmit}>
        <input name="name" onChange={handleChange} />
        <button type="submit">Send</button>
      </form>
    );
};

export default CreateItem;
