import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import {API_ROOT} from '../constants';

const renderItem = ({id, name}) => {
  return (
    <tr key={id}>
      <td>
        <b>{name}</b>
      </td>

      <td>
        <Link to={`/items/${id}/new`}>
          <button>
            Add item
          </button>
        </Link>
      </td>

      <td>
        <Link to={`/items/list_id/${id}`}>
          <button>
            Show items
          </button>
        </Link>
      </td>
    </tr>
  );
};

const Lists = () => {
  const [items, setItems] = useState(null);

  useEffect(() => {
    fetch(API_ROOT + 'lists')
      .then(response => response.json())
      .then(response => {
        setItems(response);
      })
      .catch(error => {
        throw error;
      });
  }, []);

  return items ? (
    <table>
      <tbody>{items.map(renderItem)}</tbody>
    </table>
  ) : (
    <h1>Loading</h1>
  );
};

export default Lists;
