import React, {useState, useEffect} from 'react';
import {Link, useParams} from 'react-router-dom';
import {API_ROOT} from '../constants';

const renderItem = data => {
  return (
    <tr key={data.id}>
      <td>{data.id}</td>
      <td>
        <b>{data.name}</b>
      </td>
    </tr>
  );
};

const Items = () => {
  const {list_id} = useParams();
  const [items, setItems] = useState(null);

  useEffect(() => {
    fetch(`${API_ROOT}items/list_id/${list_id}?limit=10`)
      .then(response => response.json())
      .then(response => {
        setItems(response);
      })
      .catch(error => {
        throw error;
      });
  }, []);

  return items ? (
    <div>
      <Link to="/">Go Back</Link>

      <table>
        <tbody>{items.map(renderItem)}</tbody>
      </table>
    </div>
  ) : (
    <h1>Loading</h1>
  );
};

export default Items;
