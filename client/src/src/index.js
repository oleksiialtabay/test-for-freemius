import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

const root = document.getElementById('react-root');
const rootSSR = document.getElementById('react-root__ssr');

if (rootSSR) {
  ReactDOM.hydrate(
    <App />,
    rootSSR
  );
}

if (root) {
  ReactDOM.render(
    <App />,
    root
  );
}

